class AddFieldsToDonations < ActiveRecord::Migration[5.1]
  def change
    add_column :donations, :amount, :integer
    add_column :donations, :donation_type, :string
    add_column :donations, :coin, :string
    add_column :donations, :notification_params, :text
    add_column :donations, :transaction_id, :string
    add_column :donations, :purchased_at, :datetime
  end
end
