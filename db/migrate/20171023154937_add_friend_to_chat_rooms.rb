class AddFriendToChatRooms < ActiveRecord::Migration[5.1]
  def change
    add_reference :chat_rooms, :friend, foreign_key: true
  end
end
