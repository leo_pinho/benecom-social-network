class AddStatusToOrganizationCertificates < ActiveRecord::Migration[5.1]
  def change
    add_column :organization_certificates, :status, :string
  end
end
