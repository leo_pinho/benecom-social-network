class CreateDonations < ActiveRecord::Migration[5.1]
  def change
    create_table :donations do |t|
      t.integer :user_id, :null => false
      t.integer :organization_id, :null => false
      t.string  :status

      t.timestamps
    end
  end
end
