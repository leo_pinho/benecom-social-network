class CreateOrganizationCertificates < ActiveRecord::Migration[5.1]
  def change
    create_table :organization_certificates do |t|
      t.string :name
      t.string :country
      t.string :registration_number
      t.string :primary_address
      t.string :city
      t.integer :postal_code
      t.string :phone
      t.integer :foundation_year
      t.string :website
      t.string :average_earnings
      t.text :mission
      t.references :user, foreign_key: true, index: true
      t.timestamps
    end
  end
end
