class AddAttachmentToOrganizationCertificates < ActiveRecord::Migration[5.1]
  def change
    add_attachment :organization_certificates, :constitutive_act
    add_attachment :organization_certificates, :statutes
    add_attachment :organization_certificates, :registration_certificate
  end
end
