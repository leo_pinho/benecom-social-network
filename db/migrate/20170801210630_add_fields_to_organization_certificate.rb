class AddFieldsToOrganizationCertificate < ActiveRecord::Migration[5.1]
  def change
    add_column :organization_certificates, :vision, :text
    add_column :organization_certificates, :response, :text
  end
end
