Rails.application.routes.draw do

  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  mount ActionCable.server => '/cable'
  resources :posts
  resources :usuarios, as: :users, only: [:show,:update,:edit] do
    collection do
      get 'reports', to: 'usuarios/reports'
      post 'reports', to: 'usuarios/reports'
      get 'explore', to: 'usuarios/explore'
      post 'explore', to: 'usuarios/explore'
      get 'conversations'
    end
  end
  resources :friendships, only: [:create,:update,:destroy]
  resources :likes, only: [:create,:update,:destroy]
  resources :chat_rooms, only: [:new,:create,:show,:index]
  resources :donations do
    collection do
      get 'mercadopago'
    end
  end
  resources :comments
  resources :organization_certificates do
    collection do
      get 'registration'
    end
  end

  devise_for :users, controllers:{
      omniauth_callbacks: "users/omniauth_callbacks",
      :registrations => "users/registrations"
  }

  post "/custom_sign_up", to: "users/omniauth_callbacks#custom_sign_up"
  post "/donations/:id" => "donations#show"
  post "/hook" => "donations#hook"

  get 'main/join'
  get 'main/learn'

  authenticated :user do
    root 'main#home'
  end
  unauthenticated :user do
    root 'main#unregistered'
  end

  resource :organization_registration do
    get :step1
    get :step2
    get :step3
    get :step4

    post :validate_step
  end

end
