module Wizard

  module OrganizationCertificate
    STEPS = %w(step1 step2 step3).freeze

    class Base
      include ActiveModel::Model
      attr_accessor :organization_certificate

      delegate *::OrganizationCertificate.attribute_names.map { |attr| [attr, "#{attr}="] }.flatten, to: :organization_certificate

      def initialize(organization_certificate_attributes)
        @organization_certificate = ::OrganizationCertificate.new(organization_certificate_attributes)
      end
      def edit_organization_registration(attributes,user)
        @organization_certificate = OrganizationCertificate.where(user:user)
        @organization_certificate.attributes = attributes
        @organization_certificate.save
      end
    end

    class Step1 < Base
      validates :name, presence: true
      validates :country, presence: true
      validates :primary_address, presence: true
      validates :city, presence: true
      validates :postal_code, presence: true
      validates :foundation_year, presence: true
      validates :phone, presence: true
    end

    class Step2 < Step1
      validates :vision, presence: true
      validates :registration_number, presence: true
      validates :mission, presence: true
    end

    class Step3 < Step2

    end

  end
end