$("#posts .data").append("<%= j render :partial => 'posts/post', :collection => @posts, :formats => [:html] %>")
$("#pagination").html("<%= j will_paginate @posts %>")
<% unless @posts.next_page %>
$("#pagination").remove()
<%end%>
window.loading = false