$("#comments .data").append("<%= j render :partial => 'comments/comment', :collection => @comments, :formats => [:html] %>")
$("#pagination").html("<%= j will_paginate @comments %>")
<% unless @comments.next_page %>
$("#pagination").remove()
<%end%>
window.loading = false