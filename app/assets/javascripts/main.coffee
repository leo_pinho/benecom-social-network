# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/


$(document).on "turbolinks:load", ()->
  $('.like_button').click ->
    $(this).addClass("like_clicked")
  window.loading = false
  $(document).scroll ->
    if !window.loading && $(document).scrollTop() > $(document).height() - 1000
      console.log("Cargando...")
      window.loading = true
      url = $(".next_page").attr("href")
      $.getScript url if url