$.ajaxSetup({
  dataType: 'json'
})
## SETUP FORM  EDIT AJAX - CONTROL
$(document).on "ajax:success", "form#user-edit-form", (ev,data,status,xhr)->
  $('.modal').modal('hide')
  $("#user-edit-form")[0].reset()
  $("#user-edit-form").clear_form_errors()
  # USER INFO
  $("#ajaxcall").html(data)
  # PANEL CONTENT USER NAME
  document.getElementById("content-user-name").innerHTML = $("#user-full-name").text()+"<small>Perfil</small>";
  # TOP NAVIGATION USER NAME
  clone = $("#topnav-user-name").children("#navbar-user-image")
  $("#topnav-user-name").html(clone)
  $("#topnav-user-name").append($("#user-full-name").text())
  $("#topnav-user-name").append("<span class='fa fa-angle-down'></span>")
$(document).on "ajax:error", "form#user-edit-form", (ev,data)->
  $("#user-edit-form").render_form_errors('user', $.parseJSON(data.responseText))

## POST FORM
$(document).on "ajax:success", "form#post-form", (ev,data,status,xhr)->
  $("#post-form")[0].reset()
  $("#post-form").clear_form_errors()
  $("#posts").prepend(data)
$(document).on "ajax:error", "form#post-form", (ev,data)->
  $("#post-form").render_form_errors('post', $.parseJSON(data.responseText))

## COMMENT FORM
$(document).on "ajax:success", "form#comment-form", (ev,data,status,xhr)->
  $("#comment-form")[0].reset()
  $("#comment-form").clear_form_errors()
  $("#comments").prepend(data)
$(document).on "ajax:error", "form#comment-form", (ev,data)->
  $("#comment-form").render_form_errors('comment', $.parseJSON(data.responseText))

## AVATAR EDIT FORM
$(document).on "ajax:success", "form#avatar-form", (ev,data,status,xhr)->
  $('#avatar-modal').modal('hide')
  location.reload()
$(document).on "ajax:error", "form#avatar-form", (ev,data)->
  $("#avatar-form").render_form_errors('user', $.parseJSON(data.responseText))

## ORGANIZATION REGISTRATION FORM
$(document).on "ajax:success", "form#organization_registration_form", (ev,data,status,xhr)->
  console.log(data)
  eval(data)
$(document).on "ajax:error", "form#organization_registration_form", (ev,data)->
  $("#organization_registration_form").render_form_errors('user_wizard', $.parseJSON(data.responseText))

## ORGANIZATION MISSION FORM
$(document).on "ajax:success", "form#organization_mission_form", (ev,data,status,xhr)->
  console.log(data)
  eval(data)
$(document).on "ajax:error", "form#organization_mission_form", (ev,data)->
  $("#organization_mission_form").render_form_errors('user_wizard', $.parseJSON(data.responseText))

## ORGANIZATION DOCUMENTS FORM
$(document).on "ajax:success", "form#organization_documents_form", (ev,data,status,xhr)->
  console.log(xhr.responseText)
  eval(xhr.responseText)
$(document).on "ajax:error", "form#organization_documents_form", (ev,data)->
  $("#organization_documents_form").render_form_errors('user_wizard', $.parseJSON(data.responseText))

## CHATS FORM
$(document).on "ajax:success", "form#chat_room_form", (ev,data,status,xhr)->
  $('#ModalNew').modal('hide')
  console.log(data)
  eval(data)
$(document).on "ajax:error", "form#chat_room_form", (ev,data)->
  $("#chat_room_form").render_form_errors('chat_room', $.parseJSON(data.responseText))

## FUNCTIONS TO APPLY CSS TO VALIDATIONS
$.fn.render_form_errors = (model_name, errors) ->
  form = this
  this.clear_form_errors()
  $.each(errors, (field, messages) ->
    input = form.find('input, select, textarea, textfield').filter(->
      name = $(this).attr('name')
      if name
        name.match(new RegExp(model_name + '\\[' + field + '\\(?'))
    )
    input.closest('.form-group').addClass('has-error')
    input.parent().append('<span class="help-block">' + $.map(messages, (m) -> m.charAt(0).toUpperCase() + m.slice(1)).join('<br />') + '</span>')
  )
## CLEAR FORM ERRORS
$.fn.clear_form_errors = () ->
  this.find('.form-group').removeClass('has-error')
  this.find('span.help-block').remove()
## CLEAR FORM FIELDS
$.fn.clear_form_fields = () ->
  this.find(':input','#myform')
    .not(':button, :submit, :reset, :hidden')
    .val('')
    .removeAttr('checked')
    .removeAttr('selected')
