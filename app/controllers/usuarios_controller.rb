class UsuariosController < ApplicationController
  before_action :set_user, only: [:show,:edit,:update]
  before_action :authenticate_user!, only: [:update,:show]
  before_action :authenticate_owner!, only: [:update]
  skip_before_action :verify_authenticity_token

  def show
    @posts = @user.posts.all
    @are_friends = current_user.my_friend?(@user)
    @friendships = Friendship.user_following(@user)
    @user_followers = Friendship.user_followers(@user)
    if @user.is_organization?
      @organization_certificate = OrganizationCertificate.where(user: @user).first
    end
  end
  def edit

  end
  def update
    respond_to do |format|
      if @user.update_attributes(user_params)
        format.html{ render :partial => 'user_info'}
        format.json { render json: @user.to_json }
      else
        format.html{ render json: (@user.errors).to_json, status: :unprocessable_entity }
        format.json{ render json: (@user.errors).to_json, status: :unprocessable_entity }
      end
    end
  end
  def reports
    @report_id = params[:report_id]
    @start_date = params[:startDate]
    @end_date = params[:endDate]
    if (params[:report_id] == '1')
      @report_name = 'Publicaciones Más Comentadas'
      @report = Post.where(user: current_user,:created_at => @start_date..@end_date).order(comments_count: :desc)
    elsif (params[:report_id] == '2')
      @report_name = 'Publicaciones Más Valoradas'
      @report = Post.where(user: current_user,:created_at => @start_date..@end_date).order(likes_count: :desc)
    elsif (params[:report_id] == '3')
      @report_name = 'Fondos Captados'
      @paypal_funds = Donation.where(organization: current_user,status:"Completed",:created_at => @start_date..@end_date).sum(:amount)
      @mercadopago_funds = Donation.where(organization: current_user,status:"approved",:created_at => @start_date..@end_date).sum(:amount)
    elsif (params[:report_id] == '4')
      @report_name = 'Usuarios Colaboradores'
      @report_paypal = Donation.joins("INNER JOIN users ON donations.user_id = users.id").select("users.username AS username","SUM(donations.amount) AS suma").where(organization: current_user,status:"Completed",:created_at => @start_date..@end_date).group(:user_id)
      @report_mercadopago = Donation.joins("INNER JOIN users ON donations.user_id = users.id").select("users.username AS username","SUM(donations.amount) AS suma").where(organization: current_user,status:"approved",:created_at => @start_date..@end_date).group(:user_id)
    elsif (params[:report_id] == '5')
      @report_name = 'Historial de Colaboraciones'
      @report_paypal = Donation.where(organization: current_user,status:"Completed",:created_at => @start_date..@end_date).nuevos
      @report_mercadopago = Donation.where(organization: current_user,status:"approved",:created_at => @start_date..@end_date).nuevos
    elsif (params[:report_id] == '6')
      @report_name = 'Número de Seguidores'
      @report = Friendship.where(friend:current_user,:created_at => @start_date..@end_date).count(:id)
    end
  end
  def explore
    @report_id = params[:report_id]

    if (params[:report_id] == '1')
      @report_name = 'Publicaciones Más Comentadas'
      @report = Post.where(user: current_user).order(comments_count: :desc)
    elsif (params[:report_id] == '2')
      @report_name = 'Publicaciones Más Valoradas'
      @report = Post.where(user: current_user).order(likes_count: :desc)
    elsif (params[:report_id] == '3')
      @report_name = 'Organizaciones'
      @report = User.where(is_ong: true)
    elsif (params[:report_id] == '4')
      @report_name = 'Usuarios'
      @report = User.where(is_ong: false)
    end
  end
  private
  def set_user
    @user = User.find(params[:id])
  end
  def authenticate_owner!
    if current_user != @user
      redirect_to root_path, notice: "No estas autorizado", status: :unauthorized
    end
  end
  def user_params
    params.require(:user).permit(:email,:username,:name,:last_name,:bio,:avatar)
  end
  def authenticate_organization
    if !current_user.is_organization?
      redirect_to root_path, notice: "No estas autorizado", status: :unauthorized
    end
  end
end