class FriendshipsController < ApplicationController
  before_action :find_friend

  def create
    @friendship = Friendship.new(user: current_user, friend: @friend)
    respond_to do |format|
      if @friendship.save
        @are_friends = current_user.my_friend?(@friend)
        format.html{redirect_to @friend}
        format.js{}
      else
        format.html{redirect_to @friend,notice:"Error con la solicitud de amistad"}
        format.js{redirect_to @friend,notice:"Error con la solicitud de amistad"}
      end
    end
  end

  def destroy
    @friendship = Friendship.where(user: current_user, friend: @friend).first()
    if @friendship.present?
      respond_to do |format|
        if @friendship.destroy
          format.html { redirect_to @friend }
          format.js {}
        else
          format.html { redirect_to @friend, notice:"Error con la solicitud de amistad" }
          format.js { redirect_to @friend, notice:"Error con la solicitud de amistad" }
        end

      end
    end
  end

  private
  def find_friend
    @friend = User.find(params[:friend_id])
  end

  def find_model
    @model = Friendship.find(params[:id]) if params[:id]
  end
end