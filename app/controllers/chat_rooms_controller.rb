class ChatRoomsController < ApplicationController
  before_action :authenticate_user!
  before_action :authenticate_owner!, only: [:show]

  def index
    @chat_rooms = ChatRoom.where(user:current_user)
                          .or(ChatRoom.where(friend:current_user))
  end

  def new
    @chat_room = ChatRoom.new
  end

  def create
    @chat_room = current_user.chat_rooms.build(chat_room_params)
    @chat_room.friend = User.find(params[:chat_room][:friend])
    respond_to do |format|
      if @chat_room.save
        format.html{redirect_to @chat_room}
      else
        format.html{redirect_to chat_rooms_path}
      end
    end
  end

  def show
    @chat_room = ChatRoom.includes(:messages).find_by(id: params[:id])
    @message = Message.new
  end

  private

  def chat_room_params
    params.require(:chat_room).permit(:title)
  end

  def authenticate_owner!
    @chat_room = ChatRoom.find(params[:id])
    if (current_user != @chat_room.user) && (current_user != @chat_room.friend)
      redirect_to root_path, notice: "No estas autorizado", status: :unauthorized
    end
  end
end