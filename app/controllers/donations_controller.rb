class DonationsController < ApplicationController
  before_action :set_organization, only: [:new]
  before_action :set_donation, only: [:show,:edit,:update,:destroy]

  def index
    @donations = Donation.where(user:current_user,status:"Completed")
                         .or(Donation.where(user:current_user,status:"approved"))
    @donors = Donation.where(organization:current_user,status:"Completed")
                  .or(Donation.where(organization:current_user,status:"approved"))
  end
  def show
    if (params[:collection_id])
      params.permit!
      @donation.update_attributes notification_params: params.to_h, status: params[:collection_status], transaction_id: params[:collection_id], purchased_at: Time.now
    end
  end
  def mercadopago
    require 'mercadopago.rb'
    $mp = MercadoPago.new('7017092493623180', '7QTYvrqPPwU1A50Hp6bWbrACeDA8dJJ0')

    preference_data = {
        "items": [
            {
                "title": "Donacion",
                "quantity": 1,
                "unit_price": 100,
                "currency_id": "VEF"
            }
        ],
        "back_urls":{
            "success":"http://localhost:3000"
        }
    }
    @preference = $mp.create_preference(preference_data)

    puts @preference
  end
  def new
    @donation = Donation.new
    session[:organization_id] = @user.id
  end
  def create
    @donation = Donation.new(donation_params)
    @donation.user = current_user
    @donation.organization = User.find(session[:organization_id])
    if @donation.save
      if @donation.donation_type == "paypal"
        redirect_to @donation.paypal_url(donation_path(@donation))
      else
        redirect_to @donation.mercadopago_url(donation_path(@donation))
      end
    else
      render :new
    end
  end
  protect_from_forgery except: [:hook]
  def hook
    params.permit! # Permit all Paypal input params
    status = params[:payment_status]
    if status == "Completed"
      @donation = Donation.find params[:invoice]
      @donation.update_attributes notification_params: params.to_h, status: status, transaction_id: params[:txn_id], purchased_at: Time.now
    end
    render nothing: true
  end


  private
    def set_organization
      @user = User.find(params[:organization_id])
      @organization_certificate = OrganizationCertificate.where(user: @user).first
    end
    # Use callbacks to share common setup or constraints between actions.
    def set_donation
      @donation = Donation.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def donation_params
      params.require(:donation).permit(:amount,:donation_type)
    end

end