class LikesController < ApplicationController
  before_action :find_post

  def create
    @like = Like.new(user: current_user, post: @post)
    respond_to do |format|
      if @like.save
        @user_likes = current_user.likes?(@post)
        format.html{redirect_to @post}
        format.js{}
      else
        format.html{redirect_to @post,notice:"Error con la solicitud de amistad"}
        format.js{redirect_to @post,notice:"Error con la solicitud de amistad"}
      end
    end
  end

  def destroy
    @like = Like.where(user: current_user, post: @post).first()
    if @like.present?
      respond_to do |format|
        if @like.destroy
          format.html { redirect_to @post }
          format.js {}
        else
          format.html { redirect_to @post, notice:"Error con la solicitud de amistad" }
          format.js { redirect_to @post, notice:"Error con la solicitud de amistad" }
        end

      end
    end
  end

  private
  def find_post
    @post = Post.find(params[:post_id])
  end

end