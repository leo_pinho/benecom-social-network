class OrganizationCertificatesController < ApplicationController
  before_action :set_user_organization_certificate, only: [:registration,:show]
  before_action :set_organization_certificate, only: [:request]
  before_action :set_user, only: [:request]
  before_action :authenticate_owner!, only: [:request]
  before_action :authenticate_user!, only: [:new,:create,:update]

  def index
  end

  def registration

  end
  def show

  end

  private
  def set_user_organization_certificate
    @organization_certificate= OrganizationCertificate.where(user: current_user).first
  end
  def set_organization_certificate
    @organization_certificate= OrganizationCertificate.find_by_id params[:id]
  end
  def set_user
    if (@organization_certificate != nil)
      @user = User.find_by_id(@organization_certificate.user_id)
    else
      redirect_to root_path, notice: "No estas autorizado"
    end
  end
  def authenticate_owner!
    if (current_user != @user) && (@user.nil?)
      redirect_to root_path, notice: "No estas autorizado", status: :unauthorized
    end
  end
end