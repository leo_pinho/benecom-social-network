class OrganizationRegistrationsController < ApplicationController
  before_action :authenticate_user!
  before_action :load_user_wizard, except: %i(validate_step)
  skip_before_action :verify_authenticity_token


  def validate_step
    current_step = params[:current_step]
    @user_wizard = wizard_user_for_step(current_step)
    @user_wizard.organization_certificate.attributes = user_wizard_params
    session[:user_attributes] = @user_wizard.organization_certificate.attributes
    respond_to do |format|
      if @user_wizard.valid?
        format.html{
          next_step = wizard_user_next_step(current_step)
          if (next_step)
            redirect_to action: next_step
          else
            validate = OrganizationCertificate.where(user:current_user).first
            if (validate)
              update and return
            else
              create and return
            end
          end
        }
        format.json{}
      else
        format.html{render json: (@user_wizard.errors).to_json, status: :unprocessable_entity}
        format.json{render json: (@user_wizard.errors).to_json, status: :unprocessable_entity}
      end
    end
  end

  def create
    @user_wizard.user_id = current_user.id
    if @user_wizard.organization_certificate.save
      session[:user_attributes] = nil
      redirect_to registration_organization_certificates_path, notice: 'User succesfully created!'
    else
      redirect_to registration_organization_certificates_path, alert: 'There were a problem when creating the user.'
    end
  end
  def update
    @current_registration = OrganizationCertificate.where(user:current_user).first
    organization_registration_params =session[:user_attributes].slice('name','country','mission','city','phone',
                                                                      'primary_address','website','postal_code',
                                                                      'foundation_year','vision','registration_number',
                                                                      'constitutive_act_file_name','constitutive_act_content_type','constitutive_act_file_size','constitutive_act_updated_at',
                                                                      'statutes_file_name','statutes_content_type','statutes_file_size','statutes_updated_at',
                                                                      'registration_certificate_file_name','registration_certificate_content_type','registration_certificate_file_size','registration_certificate_updated_at')
    fixed_params = transform_updated_params(user_wizard_params,organization_registration_params)

    if @current_registration.update(fixed_params)
      session[:user_attributes] = nil
      redirect_to registration_organization_certificates_path, notice: 'User succesfully created!'
    else
      redirect_to registration_organization_certificates_path, alert: 'There were a problem when creating the user.'
    end
  end

  private

  def load_user_wizard
    @user_wizard = wizard_user_for_step(action_name)
  end

  def wizard_user_next_step(step)
    Wizard::OrganizationCertificate::STEPS[Wizard::OrganizationCertificate::STEPS.index(step) + 1]
  end

  def wizard_user_for_step(step)
    raise InvalidStep unless step.in?(Wizard::OrganizationCertificate::STEPS)
    validate = OrganizationCertificate.where(user:current_user).first
    if (validate)
      if (step == "step1")
        "Wizard::OrganizationCertificate::#{step.camelize}".constantize.new(name: validate.name,primary_address: validate.primary_address,
                                                                            country:validate.country,city:validate.city,phone:validate.phone,
                                                                            postal_code:validate.postal_code,website:validate.website,
                                                                            foundation_year:validate.foundation_year)
      elsif (step == "step2")
        session[:user_attributes]['vision'] = validate.vision
        session[:user_attributes]['registration_number'] = validate.registration_number
        session[:user_attributes]['mission'] = validate.mission
        "Wizard::OrganizationCertificate::#{step.camelize}".constantize.new(session[:user_attributes])
      elsif (step == "step3")
        "Wizard::OrganizationCertificate::#{step.camelize}".constantize.new(session[:user_attributes])
      end
    else
      "Wizard::OrganizationCertificate::#{step.camelize}".constantize.new(session[:user_attributes])
    end
  end
  def user_wizard_params
    params.fetch(:user_wizard,{}).permit(:name,:mission,:registration_number,:vision,:city,:foundation_year,:postal_code,:phone,:website,:country,:primary_address,:constitutive_act,:statutes,:registration_certificate)
  end
  def transform_updated_params(params, values)
    values.each do |key, value|
      params[key] = value
    end
    return params
  end

  class InvalidStep < StandardError; end
end