# == Schema Information
#
# Table name: friendships
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  friend_id  :integer
#  status     :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Friendship < ApplicationRecord
  include AASM
  belongs_to :user
  belongs_to :friend,class_name: "User"
  validates :user_id, uniqueness:{scope: :friend_id,message:"Amistad Duplicada"}

  def self.friends?(user,friend)
    Friendship.where(user:user,friend:friend)
              .or(Friendship.where(user:friend,friend:user))
              .any?
  end

  def self.following?(user,friend)
    Friendship.where(user:user,friend:friend).any?
  end

  def self.user_following(user)
    Friendship.active.where(user: user)
  end

  def self.user_followers(user)
    Friendship.active.where(friend: user)
  end

  aasm column: "status" do
    state :pending
    state :active, initial: true
    state :denied

    event :accepted do
      transitions from: [:pending], to: [:active]
    end
    event :rejected do
      transitions from: [:pending,:active], to: [:denied]
    end
  end
end
