class Donation < ApplicationRecord
  belongs_to :user, :class_name => :User
  belongs_to :organization, :class_name => :User
  scope :nuevos, ->{order("created_at desc")}

  serialize :notification_params, Hash
  def paypal_url(return_path)
    values = {
        business: "ladepinho-facilitator-2@gmail.com",
        cmd: "_xclick",
        upload: 1,
        return: "#{Rails.application.secrets.app_host}#{return_path}",
        invoice: id,
        amount: amount,
        item_name: "donacion",
        item_number: "1",
        quantity: '1',
        notify_url: "#{Rails.application.secrets.app_host}/hook"
    }
    "#{Rails.application.secrets.paypal_host}/cgi-bin/webscr?" + values.to_query
  end
  def mercadopago_url(return_path)
    require 'mercadopago.rb'
    $mp = MercadoPago.new('7017092493623180', '7QTYvrqPPwU1A50Hp6bWbrACeDA8dJJ0')

    preference_data = {
        "items": [
            {
                "title": "Donacion",
                "invoice": id,
                "quantity": 1,
                "unit_price": amount,
                "currency_id": "VEF"
            }
        ],
        "back_urls":{
            "success":"#{Rails.application.secrets.app_host}#{return_path}"
        }
    }
    preference = $mp.create_preference(preference_data)

    return preference["response"]["sandbox_init_point"]
  end

end
