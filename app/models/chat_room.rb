class ChatRoom < ApplicationRecord
  belongs_to :user
  belongs_to :friend,class_name: "User"
  has_many :messages, dependent: :destroy
  validates :user_id, uniqueness:{scope: :friend_id,message:"Conversacion Duplicada"}

  def get_friend(user,chat_room)
    if chat_room.user == user
      return chat_room.friend
    else
      return chat_room.user
    end
  end
end
