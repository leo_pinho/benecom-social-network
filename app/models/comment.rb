class Comment < ApplicationRecord
  belongs_to :user
  belongs_to :post
  scope :nuevos, ->{order("created_at desc")}
  after_save :update_comments_count

  def update_comments_count
    updated_count = self.post.comments_count+1
    self.post.update_attributes(:comments_count => updated_count)
  end
end
