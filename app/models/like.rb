class Like < ApplicationRecord
  belongs_to :user
  belongs_to :post
  after_save :increase_likes_count
  before_destroy :decrease_likes_count

  def self.user_likes?(user,post)
    Like.where(user:user,post:post).any?
  end
  def increase_likes_count
    post = Post.find(self.post_id)
    updated_value = post.likes_count + 1
    post.update_attributes(likes_count:updated_value)
  end
  def decrease_likes_count
    post = Post.find(self.post_id)
    updated_value = post.likes_count - 1
    post.update_attributes(likes_count:updated_value)
  end
end
