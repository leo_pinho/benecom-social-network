# == Schema Information
#
# Table name: organization_certificates
#
#  id                                    :integer          not null, primary key
#  name                                  :string
#  country                               :string
#  registration_number                   :string
#  primary_address                       :string
#  city                                  :string
#  postal_code                           :integer
#  phone                                 :string
#  foundation_year                       :integer
#  website                               :string
#  average_earnings                      :string
#  mission                               :text
#  user_id                               :integer
#  created_at                            :datetime         not null
#  updated_at                            :datetime         not null
#  constitutive_act_file_name            :string
#  constitutive_act_content_type         :string
#  constitutive_act_file_size            :integer
#  constitutive_act_updated_at           :datetime
#  statutes_file_name                    :string
#  statutes_content_type                 :string
#  statutes_file_size                    :integer
#  statutes_updated_at                   :datetime
#  registration_certificate_file_name    :string
#  registration_certificate_content_type :string
#  registration_certificate_file_size    :integer
#  registration_certificate_updated_at   :datetime
#  status                                :string
#  vision                                :text
#  response                              :text
#

class OrganizationCertificate < ApplicationRecord
  include AASM
  belongs_to :user
  validates_uniqueness_of :user_id
  validates :status, presence: true
  after_initialize :init
  after_update :check_certificate_state

  has_attached_file :constitutive_act
  validates_attachment_size :constitutive_act, :less_than => 10.megabytes
  # validates_attachment_presence :constitutive_act
  validates_attachment_content_type :constitutive_act, :content_type =>['application/pdf'],
                                    :message => 'Only PDF Files are allowed.'
  has_attached_file :statutes
  validates_attachment_size :statutes, :less_than => 10.megabytes
  # validates_attachment_presence :constitutive_act
  validates_attachment_content_type :statutes, :content_type =>['application/pdf'],
                                    :message => 'Only PDF Files are allowed.'
  has_attached_file :registration_certificate
  validates_attachment_size :registration_certificate, :less_than => 10.megabytes
  # validates_attachment_presence :constitutive_act
  validates_attachment_content_type :registration_certificate, :content_type =>['application/pdf'],
                                    :message => 'Only PDF Files are allowed.'

  rails_admin do
    update do
      field :status , :enum do
        enum do
          ['Pendiente por Revisión','Validando Información','Validando Documentación','Validando Registro','Rechazada','Aprobada']
        end
      end
      field :response
    end
    list do
      field :name
      field :registration_number
      field :status
    end
  end

  def init
    self.status ||= "Pendiente por Revisión"
  end
  def check_certificate_state
    organization = User.find(self.user_id)
    if self.status == "Aprobada"
      organization.is_ong = true
    else
      organization.is_ong = false
    end
    organization.save
  end

  def self.verified?(user)
    OrganizationCertificate.where(user: user,status: 'Aprobada').any?
  end
end
